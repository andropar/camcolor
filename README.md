CAMColor
==============================

Color Detection using Class Activation Mapping for fuzzy image segmentation.

Note - the data folder structure (see below) doesn't exist, as it is ignored by Git. You will have to create the folders accordingly and put the compressed images into the folder 'raw'. Then you can run the data processing scripts in the data folder in src to create the training and validation dataset.

Project Organization
------------

    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │                     predictions
    │   │
    │   ├── visualization  <- Scripts to create exploratory and results oriented visualizations
    │   │
    │   └── utils          <- Contains helper functions


--------

Project based on the cookiecutter data science project template (https://drivendata.github.io/cookiecutter-data-science/).
