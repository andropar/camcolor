import sys
sys.path.append('../utils/')
import cv2
import glob
import os
from util import printProgressBar

base_path = os.path.dirname(os.path.dirname(os.getcwd()))
data_path = base_path + '/data'
cleaned_images_path = data_path + '/interim/reduced_categories/images'

reduced_categories = ['headwear', 'upperbody', 'lowerbody', 'fullbody', 'socks', 'shoes', 'accessoires']

for category in reduced_categories:
    print('Processing images in category: ' + category)
    category_image_paths = glob.glob(cleaned_images_path + '/' + category + '/*.jpeg')

    l = len(category_image_paths)
    printProgressBar(0, l, prefix = ' Progress: ', suffix = 'Complete', length = 50)

    for idx, image_path in enumerate(category_image_paths):

        image = cv2.imread(image_path)
        resized_image = cv2.resize(image, (448, 448), interpolation=cv2.INTER_AREA)
        cv2.imwrite(image_path, resized_image)

        printProgressBar(idx + 1, l, prefix = ' Progress: ', suffix = 'Complete', length = 50)