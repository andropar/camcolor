import os, sys
import pandas as pd
sys.path.append('../utils/')
import glob
from shutil import copyfile
from util import printProgressBar

base_path = os.path.dirname(os.path.dirname(os.getcwd()))
data_path = base_path + '/data'
raw_images_path = data_path + '/raw/images'
raw_labels_path = data_path + '/raw/images_overview.csv'
cleaned_images_path = data_path + '/interim/reduced_categories/images'
cleaned_labels_path = data_path + '/interim/reduced_categories/images_overview.csv'

reduced_categories = ['headwear', 'upperbody', 'lowerbody', 'fullbody', 'socks', 'shoes', 'accessoires']
headwear = ['Kopftuch','Cap','Ohrenwärmer','Mütze','Hut']
upperbody = ['Funktionsshirt','Langarmshirt','T-Shirt basic','Top','Poloshirt','T-Shirt print','Sweatshirt','Unterhemd / Shirt','Laufjacke','Winterjacke','Trainingsjacke','Weste','Daunenmantel','Bikini-Top','Kapuzenpullover','Fleecepullover','Sweatjacke','Sport-BH','Sport BH','Torwarttrikot','Windbreaker','Softshelljacke','Fleecejacke','Daunenjacke','Regenjacke / wasserabweisende Jacke','Bikini','Surfshirt','Jumpsuit','Leichte Jacke','Bomberjacke','Strickjacke','Wollmantel / klassischer Mantel','Kurzmantel','Strickpullover','Übergangsjacke','Outdoorjacke','Hemd','Bluse','Parka','Hardshelljacke','Trainingstrikot']
lowerbody = ['Tights','kurze Sporthose','3/4 Sporthose','Stoffhose','Jogginghose','Shorts','Bikini-Hose','Sportrock','Panties','Unterhose lang','Badehosen Pants','Badeshorts','Badehosen Slips','Leggings','Hosen','A-Linien-Rock','Bleistiftrock','Chino','Jeans Straight Leg','Schneehose','Minirock','Ballonrock','Cargohose','Boxershorts']
fullbody = ['Sportkleid','Trainingsanzug','Freizeitkleid','Jerseykleid','Badeanzug','Etuikleid','Bademantel']
socks = ['Füßlinge','Kniestrümpfe','Sportsocken','Socken']
shoes = ['Fußballschuh Halle','Fußballschuh Multinocken','Laufschuh Leichtigkeit','Tennisschuh Outdoor','Trainings- / Fitnessschuh','Laufschuh Dämpfung','Laufschuh Neutral','Tanzschuh','Laufschuh Stabilität','Sneaker low','Laufschuh Natural running','Tennisschuh Indoor','Laufschuh Trail','Laufschuh Wettkampf','Multicourt-Schuh','Sneaker high','Basketballschuh','Laufschuh Dämpfung','Laufschuh Neutral','Tanzschuh','Laufschuh Stabilität','Sneaker low','Laufschuh Natural running','Tennisschuh Indoor','Laufschuh Trail','Laufschuh Wettkampf','Badesandale','Multicourt-Schuh','Sneaker high','Basketballschuh','Tennisschuh Multicourt','Fußballschuh Nocken','Fußballschuh Stollen','Schnürstiefelette','Zehentrenner','Sneaker', 'Lauflernschuh','Krabbelschuh','Slipper','Golfschuh','Skaterschuh','Skateschuh','Trekkinghalbschuh','Handballschuh','Volleyballschuh','Wassersportschuh','Trekkingboot','Hikingschuh','Snowboot / Winterstiefel','Spikes','Kletterschuh','Walkingschuh','Bade-Zehentrenner']
accessoires = ['Fingerhandschuh','Sporttasche','Schlauchschal','Schweißband','Tagesrucksack','Accessoires Sonstiges','Umhängetasche','Protektoren / Schoner','Fanartikel','Stutzen','Torwarthandschuhe','Schienbeinschoner','Basketball','Geldbörse','Handtuch','Gürteltasche','Kurzfingerhandschuh','Trinkflasche','Sportbeutel','Fußball','Schal','Zubehör','Bandagen','Balltasche','Schuhtasche','Shopping Bag','Gürtel','Sonnenbrille','Digitaluhr','Uhr','Handtasche','Strandaccessoire', 'Fäustlinge','Kosmetiktasche','Fußball Sonstiges','Trolley','Trekkingrucksack','Handball','Schwimmbrille']

def main():
    df = pd.read_csv(raw_labels_path)
    df_noNA = df.dropna(subset=['color'])

    for category in reduced_categories:
        category_list = globals()[category]
        df_noNA.loc[df_noNA['producttype'].isin(category_list), 'producttype'] = category

    df_noNA.to_csv(cleaned_labels_path)

    for category in reduced_categories:
        category_path = cleaned_images_path + '/' + category
        if not os.path.exists(category_path):
            os.makedirs(category_path)

    print('Copying images into corresponding category folders:')
    l = len(df_noNA)
    printProgressBar(0, l, prefix=' Progress: ', suffix='Complete', length=50)
    for idx, row in df_noNA.iterrows():
        if row.producttype in reduced_categories:
            filename_raw = raw_images_path + '/' + str(row.id) + '.jpeg'
            filename_new = cleaned_images_path + '/' + row.producttype + '/' + str(row.id) + '.jpeg'
            copyfile(filename_raw, filename_new)
            printProgressBar(idx + 1, l, prefix=' Progress: ', suffix='Complete', length=50)

if __name__ == '__main__':
    main()