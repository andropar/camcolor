import sys
sys.path.append('../utils/')
import cv2
import glob
import os
from shutil import copyfile
import numpy as np
from util import *

base_path = os.path.dirname(os.path.dirname(os.getcwd()))
data_path = base_path + '/data'
cleaned_images_path = data_path + '/interim/reduced_categories/images'
sample_images_path = data_path + '/sample/reduced_categories/images'

reduced_categories = ['headwear', 'upperbody', 'lowerbody', 'fullbody', 'socks', 'shoes', 'accessoires']

createDirIfNotExists(sample_images_path)

def splitTrainValid(category):

    train_path, valid_path = createTrainValidFolderStructure(sample_images_path, category)

    for dirname in os.listdir(cleaned_images_path):
        processed_images = np.random.permutation(glob.glob(cleaned_images_path + '/' + dirname + '/*.jpeg'))
        if(dirname == category):
            for i in range(round(len(processed_images) / 10)):
                path, image = os.path.split(processed_images[i])
                copyfile(processed_images[i], train_path + '/yes/' + image)
        else:
            for i in range(round(len(processed_images) / 10)):
                path, image = os.path.split(processed_images[i])
                copyfile(processed_images[i], train_path + '/no/' + image)

    yes_images = np.random.permutation(glob.glob(train_path + '/yes/*.jpeg'))
    no_images = np.random.permutation(glob.glob(train_path + '/no/*.jpeg'))

    for i in range(round(len(yes_images) / 5)):
        path, image = os.path.split(yes_images[i])
        os.rename(yes_images[i], valid_path + '/yes/' + image)

    for i in range(round(len(no_images) / 5)):
        path, image = os.path.split(no_images[i])
        os.rename(no_images[i], valid_path + '/no/' + image)

def main():
    for category in reduced_categories:
        splitTrainValid(category)

if __name__ == "__main__":
    main()