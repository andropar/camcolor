from keras import applications
from keras.models import Model
from keras.layers.core import Dense, Lambda
from keras.optimizers import Adam
from keras import backend as K

def get_model():
    base_model = applications.vgg16.VGG16(include_top=True, weights='imagenet', input_shape=(224, 224, 3))

    base_model.layers.pop()

    for layer in base_model.layers:
        layer.trainable = False

    for layer in base_model.layers[-2:]:
        layer.trainable = True


    predictions = Dense(2, activation='softmax') (base_model.layers[-1].output)
    model = Model(input=base_model.input, output = predictions)

    model.compile(optimizer=Adam(lr=0.01), loss='categorical_crossentropy', metrics=['accuracy'])

    return model
