import os
from clr_callback import CyclicLR
import base_model
from keras import backend as K
from keras.preprocessing import image

#K.set_image_dim_ordering('tf')

category = 'lowerbody'

base_path = os.path.dirname(os.path.dirname(os.getcwd()))
data_path = base_path + '/data'
images_path = data_path + '/processed/reduced_categories/images/' + category
train_path = images_path + '/train'
valid_path = images_path + '/valid'
model_path = base_path + '/models/vgg16_' + category


def get_batches(path, gen=image.ImageDataGenerator(), shuffle=True, batch_size=8, class_mode='categorical'):
    return gen.flow_from_directory(path, target_size=(224, 224), class_mode=class_mode, shuffle=shuffle,
                                  batch_size=batch_size)

def main():
    gen_t = image.ImageDataGenerator(rotation_range=15, height_shift_range=0.05,
                                     shear_range=0.1, width_shift_range=0.1)

    train_batches = get_batches(train_path, gen_t, batch_size=32)
    valid_batches = get_batches(valid_path, batch_size=16)

    model = base_model.get_model()

    clr = CyclicLR(base_lr=0.001, max_lr=0.006, step_size=2000.)

    for i in range(5):
        print('Epoch ' + str(i) + ' of 1')
        model.fit_generator(train_batches, samples_per_epoch=train_batches.nb_sample, nb_epoch=1,
                            validation_data=valid_batches, nb_val_samples=valid_batches.nb_sample,
                            callbacks=[clr])
        model.save_weights(model_path + '/weights_' + category + '_ep' +  str(i) + '.h5')
    return

if __name__ == "__main__":
    main()