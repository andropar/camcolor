from keras import applications
from keras.models import Model
from keras.layers.core import Dense, Lambda
from keras.optimizers import Adam
from keras import backend as K

def global_average_pooling(x):
    return K.mean(x, axis=(2, 3))

def global_average_pooling_shape(input_shape):
    return input_shape[0:2]

def get_model():
    base_model = applications.vgg16.VGG16(include_top=True, weights='imagenet', input_shape=(224, 224, 3))

    for i in range(5):
        base_model.layers.pop()

    for layer in base_model.layers:
        layer.trainable = False

    for layer in base_model.layers[-1:]:
        layer.trainable = True

    avg_pooling = Lambda(global_average_pooling, output_shape=global_average_pooling_shape) (base_model.layers[-1].output)
    predictions = Dense(2, activation='softmax') (avg_pooling)
    model = Model(input=base_model.input, output = predictions)

    model.compile(optimizer=Adam(lr=0.01), loss='categorical_crossentropy', metrics=['accuracy'])

    return model

