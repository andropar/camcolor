import sys
sys.path.append('utils/')
sys.path.append('models/')
sys.path.append('visualization/')
import grad_cam
import base_model_with_fc
import base_model
import util
import cv2
import keras.backend as K
import glob
import numpy as np
import matplotlib.pyplot as plt

def test_model(category, img_path, with_fc):
    if with_fc:
        model_path = '../models/vgg16_' + category + '/weights_ep0_with_fc.h5'
        model = base_model_with_fc.get_model()
    else:
        model_path = '../models/vgg16_' + category + '/weights_' + category + '_best.h5'
        model = base_model.get_model()
    model.load_weights(model_path)

    img, heatmap, superimposed_img, preds = grad_cam.getGradCAM(model, img_path)

    return img, heatmap, superimposed_img, preds



def main():
    category = 'lowerbody'
    img_paths = np.random.permutation(glob.glob('../data/processed/reduced_categories/images/' + category + '/train/yes/*.jpeg'))
    with_fc = False
    result_list = []

    for img_path in img_paths[:10]:
        img, heatmap, superimposed_img, preds = test_model(category, img_path, with_fc)
        result_list.append({'superimposed_img': superimposed_img, 'preds': preds})

    fig = plt.figure(figsize=(16, 16))

    for i in range(1, 11):
        subplt = fig.add_subplot(5, 2, i)
        subplt.set_title(result_list[i-1]['preds'])
        plt.imshow(cv2.cvtColor(result_list[i-1]['superimposed_img'], cv2.COLOR_BGR2RGB))

    plt.show()

if __name__ == '__main__':
    main()