import sys
sys.path.append('../utils/')
sys.path.append('../models/')
from keras.models import *
from keras.callbacks import *
import keras.backend as K
from keras.applications.vgg16 import preprocess_input
from base_model import *
import cv2
from keras.preprocessing import image
from util import *
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from skimage.segmentation import slic
import matplotlib.colors as mcolors
from PIL import Image

def getGradCAM(model, img_path):

    original_img = cv2.imread(img_path, 1)
    width, height, _ = original_img.shape

    img = image.load_img(img_path, target_size=(224, 224))
    #img = cv2.resize(original_img, (224, 224))

    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)

    preds = model.predict(x)

    class_idx = np.argmax(preds[0])
    class_output = model.output[:, class_idx]
    last_conv_layer = model.get_layer("block5_conv3")

    grads = K.gradients(class_output, last_conv_layer.output)[0]

    pooled_grads = K.mean(grads, axis = (0, 1, 2))
    iterate = K.function([model.input], [pooled_grads, last_conv_layer.output[0]])

    pooled_grads_value, conv_layer_output_value = iterate([x])

    for i in range(512):
        conv_layer_output_value[:, :, i] *= pooled_grads_value[i]

    heatmap = np.mean(conv_layer_output_value, axis=-1)

    heatmap = heatmap * -1

    heatmap = np.maximum(heatmap, 0)
    heatmap /= np.max(heatmap)

    img = cv2.imread(img_path)
    heatmap = cv2.resize(heatmap, (img.shape[1], img.shape[0]))
    heatmap = np.uint8(255 * heatmap)
    heatmap = cv2.applyColorMap(heatmap, cv2.COLORMAP_JET)
    superimposed_img = cv2.addWeighted(img, 0.6, heatmap, 0.4, 0)

    return original_img, heatmap, superimposed_img, preds


def getColorsFromGradCAM(img, heatmap):
    # Get all pixels where value of R in heatmap is greater than 5 (arbitrarily chosen)
    relevant_pixels = []
    for x, row in enumerate(heatmap):
        for y, pixel in enumerate(row):
            # 3rd position, because of BGR color space
            if pixel[2] > 100 and pixel[1] < 100:
                relevant_pixels.append((x, y))

    # Segment image into superpixels
    segments = slic(img, n_segments=100, sigma=5, compactness=20)

    relevant_segments = []
    for pixel in relevant_pixels:
        if segments[pixel[0], pixel[1]] not in relevant_segments:
            relevant_segments.append(segments[pixel[0], pixel[1]])

    relevant_colors = []
    for segment in relevant_segments:
        color = get_average_color(segments, segment, img)
        relevant_colors.append(color)

    clt = KMeans(n_clusters=2)
    clt.fit(relevant_colors)
    clt.cluster_centers_

    product_colors = []
    for i in range(clt.n_clusters):
        if not isSkinColor(clt.cluster_centers_[i]):
            product_colors.append(clt.cluster_centers_[i] / 255)

    return product_colors

def main():
    model_path = '../../models/vgg16_upperbody/weights_upperbody_best.h5'
    img_path = '../../data/test/1.jpeg'
    model = get_model()
    model.load_weights(model_path)
    print(model.summary())

    img, heatmap, superimposed_img, preds = getGradCAM(model, img_path)
    product_colors = getColorsFromGradCAM(img, heatmap)

    # Visualize heatmap on image
    cv2.imshow("Original", img)
    cv2.imshow("GradCam", superimposed_img)
    cv2.waitKey(0)

    print(product_colors)
    my_cmap = mcolors.ListedColormap(product_colors)

    plt.figure(figsize=(20, 0.5))
    plt.title('My own color map*')
    plt.pcolormesh(np.arange(my_cmap.N).reshape(1, -1), cmap=my_cmap)
    plt.gca().yaxis.set_visible(False)
    plt.gca().set_xlim(0, my_cmap.N)
    plt.show()

if __name__ == "__main__":
    main()


