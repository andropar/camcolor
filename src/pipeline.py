import sys
sys.path.append('utils/')
sys.path.append('models/')
sys.path.append('visualization/')
import grad_cam
import base_model
import util
import cv2

def main(img_path):
    categories = ['headwear', 'upperbody', 'lowerbody', 'fullbody', 'socks', 'shoes', 'accessoires']
    model = base_model.get_model()

    # Get predictions for all possible categories
    predictions = []
    for category in categories:
        category_model_path = '../models/vgg16_' + category + '/weights_' + category + '_best.h5'
        model.load_weights(category_model_path)
        img, heatmap, superimposed_img, preds = grad_cam.getGradCAM(model, img_path)
        if preds is not None:
            predictions.append({
                'category': category,
                'img': img,
                'heatmap': heatmap,
                'superimposed_img': superimposed_img,
                'preds': preds
            })

    # Get colors for predicted categories
    colors = []
    for prediction in predictions:
        product_colors = grad_cam.getColorsFromGradCAM(prediction['img'], prediction['heatmap'])
        colors.append({
            'category': prediction['category'],
            'colors': product_colors
        })

    for color in colors:
        print('Found product of category ' + color['category'] + ' with color ' + str(color['colors']) + ' in picture.')

if __name__ == '__main__':
    main('../data/test/18.jpeg')