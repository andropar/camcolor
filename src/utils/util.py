import os
from colormath.color_objects import sRGBColor, LabColor
from colormath.color_conversions import convert_color
from colormath.color_diff import delta_e_cie2000

caucasian_skin_pallete = [
    (255, 224, 189),
    (255, 205, 148),
    (234, 192, 134),
    (255, 173, 96),
    (255, 227, 159)
]

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

def get_layer(model, layer_name):
    layer_dict = dict([(layer.name, layer) for layer in model.layers])
    layer = layer_dict[layer_name]
    return layer

def createDirIfNotExists(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)

def createTrainValidFolderStructure(images_path, category):
    category_path = images_path + '/' + category
    train_path = category_path + '/train'
    valid_path = category_path + '/valid'

    createDirIfNotExists(category_path)
    createDirIfNotExists(train_path)
    createDirIfNotExists(valid_path)
    createDirIfNotExists(train_path + '/yes')
    createDirIfNotExists(valid_path + '/yes')
    createDirIfNotExists(train_path + '/no')
    createDirIfNotExists(valid_path + '/no')

    return train_path, valid_path

# Returns amount of segments
def getNumberOfSegments(segments):
    max = 0
    for row in segments:
        if row.max() > max: max = row.max()
    return max


def getAllPixelsForSuperpixel(superpixelNr, segments):
    pixels = []

    for x, row in enumerate(segments):
        for y, pixel in enumerate(row):
            if pixel == superpixelNr:
                pixels.append((x, y))

    return pixels

# Returns average color of segment, given all segment, the segment number and the
# corresponding image
def get_average_color(segments, superpixelNr, image):
    pixels = getAllPixelsForSuperpixel(superpixelNr, segments)

    r, g, b = 0, 0, 0
    count = 0
    for pixel in pixels:
        pixlr, pixlg, pixlb = image[pixel[0], pixel[1]]
        r += pixlr
        g += pixlg
        b += pixlb
        count += 1
    return ((r / count), (g / count), (b / count))

def getColorDistance(rgb1, rgb2):
    color1 = sRGBColor(rgb1[0], rgb1[1], rgb1[2])
    color2 = sRGBColor(rgb2[0], rgb2[1], rgb2[2])
    color1_lab = convert_color(color1, LabColor)
    color2_lab = convert_color(color2, LabColor)
    return delta_e_cie2000(color1_lab, color2_lab)

def isSkinColor(color):
    is_skin_color = False
    for skin_color in caucasian_skin_pallete:
        if getColorDistance(color, skin_color) < 20:
            is_skin_color = True
    return is_skin_color
